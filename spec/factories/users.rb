FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "USER_NAME#{n}"}
    sequence(:fullname) { |n| "FULL_NAME#{n}"}
    sequence(:email) { |n| "TEST#{n}@example.com"}
  end
end