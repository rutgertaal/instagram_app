require 'rails_helper'

RSpec.describe User, type: :model do

  describe 'validation' do
    let(:user) { create(:user) }

    subject { user.valid? }

    it 'should require email, username and fullname' do
      is_expected.to eq true
    end

  end
end