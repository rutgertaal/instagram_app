Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    registrations: 'users/registrations'
  }

  devise_scope :user do
    root to: "devise/sessions#new"
  end

  resources :users, except: :destroy do
    member do
      get :following, :followers
    end
  end
  resources :posts  do
    resources :likes, only: [:create, :destroy]
    resources :comments, only: [:create, :index, :destroy]
  end
  resources :relationships, only: [:create, :destroy]
  resources :activities, only: [:index]
end
