class CommentsController < ApplicationController
  before_action :find_post

  def index
    @comments = @post.comments.order(created_at: :desc)
  end

  def create
    @comment = @post.comments.build(comment_params)
    @comment.user = current_user
    if @comment.save
      @comment.create_activity :create, owner: current_user, recipient: @post.user
    else
      flash[:danger] = @comment.errors.full_messages.first
    end
    render :index
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    render :index
  end

  private

  def find_post
    @post = Post.find(params[:post_id])
  end

  def comment_params
    params.require(:comment).permit(:body, :post_id, :user_id)
  end

end