class PostsController < ApplicationController

  def index
    @users = User.all
    @already_following = User.where(id: [current_user.id, current_user.following.pluck(:id)]).pluck(:id)

    if params[:description].present?
      @title = "Search"
      @feed = Post.where('lower(description) like lower(?)', "%#{params[:description]}%").paginate(page: params[:page], per_page: 10)
    else
      @title = "Feed"
      @feed = Post.where(user_id: current_user.following_ids).order('created_at DESC').paginate(page: params[:page], per_page: 10)
    end
  end

  def show
    @title = "Post"
    @post = Post.find(params[:id])
  end

  def new
    @title = "New Post"
    @post = current_user.posts.new
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "Post created!"
      redirect_to post_path(@post.id)
    else
      flash[:danger] = @post.errors.full_messages.first
      render "new"
    end
  end

  def edit
  end

  def update
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to user_path(current_user)
  end

  private
  def post_params
    params.require(:post).permit(:id, :description, :post_image)
  end

end