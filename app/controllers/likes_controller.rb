class LikesController < ApplicationController
  before_action :find_post

  def create
    @like = Like.create(user_id: current_user.id, post_id: params[:post_id])
    @like.create_activity :create, owner: current_user, recipient: @post.user
    @post.reload
  end

  def destroy
    like = Like.find_by(user_id: current_user.id, post_id: params[:post_id])
    like.destroy
    @post.reload
  end

  private

  def find_post
    @post = Post.find(params[:post_id])
  end
end