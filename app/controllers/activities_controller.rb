class ActivitiesController < ApplicationController

  def index
    @title = "Notifications"
    @activities = PublicActivity::Activity.where(recipient: current_user).order(created_at: :desc).paginate(page: params[:page], per_page: 10)
    Activity.where(recipient: current_user).mark_as_read! :all, for: current_user
  end
end