class UsersController < ApplicationController

  # def index
  # end

  def show
    @title = "User"
    @user = User.find(params[:id])
    @posts = @user.posts.paginate(page: params[:page], per_page: 9)
  end

  # def new
  # end

  def profile
    redirect_to user_path(current_user)
  end

  def edit
    @title = "Edit User"
    @user = current_user
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      flash[:success] = "Your account was updated successfully"
      redirect_to user_path(@user)
    else
      flash[:danger] = @user.errors.full_messages.first
      render "edit"
    end
  end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page], per_page: 10)
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page], per_page: 10)
    render 'show_follow'
  end

  private

  def user_params
    params.require(:user).permit(:id, :username, :email, :password,
      :bio, :fullname, :website, :phone_number, :gender, :image, :profile_image)
  end
end
