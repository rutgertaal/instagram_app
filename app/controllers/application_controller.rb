class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
       devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:fullname, :gender, :profile_image, :phone_number,
        :website, :bio, :username, :email, :password)}
  end

  def after_sign_in_path_for(resource)
    posts_path
  end
end
