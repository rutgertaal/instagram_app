class User < ApplicationRecord
  acts_as_reader
  has_one_attached :profile_image
  has_many :posts, dependent: :destroy
  has_many :active_relationships, foreign_key: "follower_id", class_name: "Relationship", dependent: :destroy
  has_many :passive_relationships, foreign_key: "followed_id", class_name: "Relationship", dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  has_many :likes, dependent: :destroy
  has_many :comments, dependent: :destroy

  validate :image_invalid_type, :image_too_big
  validates :username, :fullname, :email, presence: true
  validates :username, uniqueness: true
  validates_length_of :username, :fullname, minimum: 6, maximum: 50
  validates_length_of :phone_number, maximum: 11
  validates_length_of :email, :website, maximum: 250
  validates_length_of :bio, maximum: 150

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :omniauthable, :omniauth_providers => [:facebook]

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def self.from_omniauth(auth)
    find_or_create_by(provider: auth.provider, uid: auth.uid) do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.username = auth.info.name
      user.image = auth.info.image
      user.fullname = auth.info.name
    end
  end

  # Follows a user
  def follow(other_user)
    following << other_user
  end

  # Unfollows a user
  def unfollow(other_user)
    following.delete(other_user)
  end

  # Returns true if the current is following the other user
  def following?(other_user)
    following.include?(other_user)
  end

  private

  def image_invalid_type
    return unless profile_image.attached?

    unless profile_image.content_type == "image/png" || profile_image.content_type == "image/jpeg"
      errors.add(:profile_image, "can only be jpeg or png")
    end
  end

  def image_too_big
    return unless profile_image.attached?

    errors.add(:profile_image, "is too big") if profile_image.blob.byte_size > 2000000
  end
end
