class Comment < ApplicationRecord
  include PublicActivity::Common
  belongs_to :user
  belongs_to :post

  validates :body, presence: true
  validate :comment_exceed_length

  private

  def comment_exceed_length
    errors.add(:body, "exceeds maximum character length") if body.length > 1000
  end
end
