class Activity < PublicActivity::Activity
  acts_as_readable on: :created_at
  belongs_to :user
end