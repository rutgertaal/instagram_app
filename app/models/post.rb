class Post < ApplicationRecord
  belongs_to :user
  has_one_attached :post_image
  has_many :likes, dependent: :destroy
  has_many :comments, dependent: :destroy

  validate :image_not_empty, :image_invalid_type, :image_too_big
  validates_length_of :description, minimum: 1, maximum: 1000

  private

  def image_not_empty
    errors.add(:post_image, "is missing!") unless post_image.attached?
  end

  def image_invalid_type
    return unless post_image.attached?

    unless post_image.content_type == "image/png" || post_image.content_type == "image/jpeg"
      errors.add(:post_image, "can only be jpeg or png")
    end
  end

  def image_too_big
    return unless post_image.attached?

    errors.add(:post_image, "is too big") if post_image.blob.byte_size > 2000000
  end
end