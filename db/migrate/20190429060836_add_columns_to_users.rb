class AddColumnsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :string
    add_column :users, :username, :string
    add_column :users, :fullname, :string
    add_column :users, :gender, :integer, default: 0
    add_column :users, :phone_number, :string
    add_column :users, :website, :text
    add_column :users, :bio, :text
    add_column :users, :image, :text
  end
end
