class CreatePostTable < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.text :description
      t.text :image
      t.references :user, foreign_key: true

      t.timestamps null: false
    end
  end
end
