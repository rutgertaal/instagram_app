class ChangeGenderTypeFromUsers < ActiveRecord::Migration[5.2]
  def up
    change_column :users, :gender, :string, default: "Not Specified"
  end

  def down
    change_column :users, :gender, :string
  end
end
